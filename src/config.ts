import * as vscode from 'vscode';

export class Config {
    private configName = "IDGen";

    public GetConfig(editor: vscode.TextEditor): vscode.WorkspaceConfiguration|undefined {
        const resource = editor.document.uri;
        if (resource.scheme != 'file') {
            return vscode.workspace.getConfiguration(this.configName);
        }
        return vscode.workspace.getConfiguration(this.configName, resource);
    }
}