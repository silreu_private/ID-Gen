import * as vscode from 'vscode';

let prefixName = 'Prefix';
let prefixLenName = 'PrefixLength';

export class Prefix {

    public async fromInput(): Promise<string | undefined> {
        let prefix = await vscode.window.showInputBox();
        return prefix;
    }
 
    public fromContext(config : vscode.WorkspaceConfiguration): string {
       let prefix = config.get<string>(prefixName) 
       return (prefix ? prefix.toUpperCase() + "-" : "");
    }

    public workspacePrefix(editor :vscode.TextEditor, config : vscode.WorkspaceConfiguration): string {
        var doc = editor.document;
        if (doc === undefined) {
            return "";
        }
        var docUri = vscode.workspace.getWorkspaceFolder(doc.uri);
        if (docUri === undefined) {
            return "";
        }
        var prefix = docUri.name;
        var prefixLen = this.getOrDefault(config.get<number>(prefixLenName), 0);
        return prefix.substr(0, prefixLen).toUpperCase();
    }

    private getOrDefault<T>(toCheck : T | undefined, d : T) : T {
        if (toCheck !== undefined) {
            return toCheck;
        }
        return d;
    }
}